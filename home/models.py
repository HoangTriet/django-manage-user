from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models


# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, email, password = None, is_staff = False, is_admin = False, is_active = True):
        if not email:
            raise ValueError("User must have an email address")
        if not password:
            raise ValueError("User must have an password")
        user_obj = self.model(
            email = self.normalize_email(email)
        )
        user_obj.set_password(password) #change user password
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.active = is_active
        user_obj.save(using=self._db)
        user_obj.create_user(email=email, password=password)
        return user_obj
    def create_staffuser(self, email, password = None):
        user_obj = self.create_user(
                    email,
                    password = password,
                    is_staff = True
        )
        return  user_obj
    def create_superuser(self, email, password = None):
        user_obj = self.create_user(
                    email,
                    password = password,
                    is_staff = True,
                    is_admin = True
        )
        return  user_obj

class User(AbstractBaseUser, PermissionsMixin):
    email       = models.EmailField(max_length=255, unique=True)
    active      = models.BooleanField(default=True)
    staff       = models.BooleanField(default=False) #staff not superuser
    admin       = models.BooleanField(default=False) #superuser
    timestamp   = models.DateTimeField(auto_now_add=True)
    USERNAME_FIELD = 'email' #username

    object = UserManager()
    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True
    @property
    def is_staff(self):
        return self.staff

    @property
    def is_active(self):
        return self.active

    @property
    def is_admin(self):
        return self.admin

class Profile(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    name        = models.CharField(max_length=100)
    phone       = models.CharField(max_length=100, null = True)
    birthday    = models.DateField()
    image       = models.ImageField(null = True)
    slary       = models.IntegerField()
    bankaccount = models.CharField(max_length=100)