from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.home, name='home'),
    path('manage/', views.manage, name='manage'),
    path('list/', views.index, name='index'),
    path('login/', auth_views.LoginView.as_view(template_name= 'pages/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name= 'pages/home.html'), name='logout'),
    path('create/', views.UserCreate.as_view(template_name= 'pages/create.html'), name='contact_create'),
    path('edit/<int:pk>', views.UserUpdate.as_view(template_name= 'pages/edit.html'), name='edit'),
    path('delete/<int:id>', views.destroy),
]