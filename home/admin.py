from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import Profile
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# Register your models here.
class ProfileInLine(admin.TabularInline):
    model = Profile

from .forms import UserAdminCreationForm, UserAdminChangeForm
from .models import User

class ProfileInline(admin.TabularInline):
    model = Profile

class UserAdmin(BaseUserAdmin):

   form = UserAdminChangeForm
   add_form = UserAdminCreationForm

   list_display = ('email', 'admin')
   list_filter = ('admin',)
   fieldsets = (
       (None, {'fields': ('email', 'password')}),
       ('Personal info', {'fields': ()}),
       ('Permissions', {'fields': ('admin',)}),
   )
   add_fieldsets = (
       (None, {
           'classes': ('wide',),
           'fields': ('email', 'password1', 'password2')}
       ),
   )

   inlines = [ProfileInLine]
   search_fields = ('email',)
   ordering = ('email',)
   filter_horizontal = ()



admin.site.register(User, UserAdmin)