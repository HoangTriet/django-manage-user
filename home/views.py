from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import get_user_model
from .models import Profile
from django.views.generic.edit import CreateView, UpdateView
from .forms import UserAdminCreationForm, UserAdminChangeForm
User = get_user_model()


def home(request):
    return render(request, 'pages/home.html')


def manage(request):
    data = {'Users': User.object.all().order_by("-id")[:8:1],
            'LastUser': User.object.filter().order_by('-id')[0],
            'Profile': Profile.objects.filter().order_by('-id')[0]}
    return render(request, 'pages/manageuser.html', data)


def index(request):
    data = {'Users': User.object.all().order_by("-id")[:13:1],}
    return render(request, 'pages/list.html', data)


def login(request):
    return render(request, 'pages/login.html')


def edit(request, pk):
    user = User.object.get(pk=pk)
    return render(request, 'pages/edit.html', {'user': user})


def destroy(id):
    user = User.object.get(id=id)
    user.delete()
    return redirect("/list")


class UserUpdate(UpdateView):
    model = User
    form_class = UserAdminCreationForm

    def get_success_url(self):
        return reverse('index')


class UserCreate(CreateView):
    model = User
    form_class = UserAdminCreationForm

    def get_success_url(self):
        return reverse('index')